\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {british}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{NEMESIS and c-NEMESIS}{10}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Simulations}{18}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Results}{23}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{Conclusions and Future work}{35}{0}{5}
