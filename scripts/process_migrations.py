import math 
import pandas as pd


#To use in all the methods
csv_separator = ';'
linebreak = '\n'
baseline_1 = 'Baseline_1'


# Return the percentiles    
def q25(x):
    return x.quantile(0.25)

def q50(x):
    return x.quantile(0.5)

def q75(x):
    return x.quantile(0.75)

def q90(x):
    return x.quantile(0.9)


def convert_joules_to_WattHour(value):
    return value / (60.0*60.0) #60 seconds * 60 minutes

def analise_migrations_no_congestion(migrations_file):
    data = []
    total = 0
    wasted_brown = 0
    p_core = 20.5    
    wasted_green = 0
    total_time= 0
    max_abs = 0.0
    avg_abs = 0.0
    max_rel = 0.0
    avg_rel = 0.0

    migrations_extra_time = open("results/migrations_extra_time.csv", "w")        
    
    # Generating the header    
    migrations_extra_time.write('extra_seconds\n')

    with open(migrations_file, encoding="UTF-8") as mig_file:    
        cursor = 0        
        for line_number, line in enumerate(mig_file):            
            row = line.split(';')
            duration_with_congestion    = float(row[1])
            duration_without_congestion = float(row[3])
            cores                       = float(row[6])
            diff = duration_with_congestion-duration_without_congestion;
            diff_relative = duration_with_congestion/duration_without_congestion
                
            if( diff >0):
                wasted_brown += diff*p_core * cores
                wasted_green += diff*p_core 
                total_time+=diff            
                total += diff            
                data.append([diff,diff_relative])            
                migrations_extra_time.write(str(diff)+'\n')  
            cursor += len(line)
    
    migrations_extra_time.close()            
    wasted_brown = convert_joules_to_WattHour(wasted_brown)
    wasted_green = convert_joules_to_WattHour(wasted_green)
    print(f'total seconds wasted {total}')
    print(f'wasted energy: source {wasted_brown} Wh dest {wasted_green}')
    print(f'total wasted energy: {wasted_brown+wasted_green} Wh')
    max_abs,avg_abs,max_rel,avg_rel = analise_extra_time(data)        

    return wasted_brown,wasted_green,max_abs,avg_abs,max_rel,avg_rel



def analise_extra_time(data):    
    col_names = ['Extra_Time_abs','Extra_Time_relative']                 
    df = pd.DataFrame(data, columns = col_names, dtype = float)
    max_abs = df['Extra_Time_abs'].max()    
    avg_abs = df['Extra_Time_abs'].mean()
    max_rel = df['Extra_Time_relative'].max()    
    avg_rel = df['Extra_Time_relative'].mean()    
    print_stats(df,'Extra_Time_abs')    
    print_stats(df,'Extra_Time_relative')
    return max_abs,avg_abs,max_rel,avg_rel
    

def print_stats(df,col):    
    print(f"Statistics for {col}")
    print('total',df[col].count())
    print('max',df[col].max())
    print('avg',df[col].mean())
    print('q25',q25(df[col]))
    print('q50',q50(df[col]))
    print('q75',q75(df[col]))
    print('q90',q90(df[col]))

def get_stats_estimations(data):        
    col_names = ['vm_id','real','estimation','abs','rel']                 
    df = pd.DataFrame(data, columns = col_names, dtype = float)
    print_stats(df,'abs')
    print_stats(df,'rel')


def analise_estimations(migrations_file):
    intra_dc_migrations = []
    inter_dc_migrations = []
    migs_data = []
    cont_inter =0
    cont_intra = 0
    cont_inter_under =0
    cont_intra_under = 0
    mape  = 0.0
    rmse  = 0.0
    with open(migrations_file, encoding="UTF-8") as mig_file:        
        for line_number, line in enumerate(mig_file):            
            if(line_number < 1):
                continue                
            row = line.split(';')
            register = []
            vm_id            = row[0]
            source           = row[1]
            dest             = row[2]
            duration         = float(row[5])
            worse_estimation = float(row[6])
            best_estimation  = float(row[7])            
            dc_sending = source.split('.')[1]
            dc_receiving = dest.split('.')[1]

            estimation = worse_estimation
            
            if(dc_sending == dc_receiving):
                estimation = best_estimation
            
            mig_row = []
            mig_row.append(duration)
            mig_row.append(estimation)
            migs_data.append(mig_row)
            if(duration > estimation and estimation != 0):
                
                register.append(vm_id)
                register.append(duration)
                register.append(estimation)
                register.append(duration - estimation)
                register.append(duration / estimation)
                
                if dc_sending == dc_receiving:
                    intra_dc_migrations.append(register)
                    cont_intra +=1
                    
                else:
                    inter_dc_migrations.append(register)
                    cont_inter+=1


        print(f'migrations understimated inter: {cont_inter}')                     
        if(cont_inter>0):
            get_stats_estimations(inter_dc_migrations)

        print(f'migrations understimated intra {cont_intra}')         
        if(cont_intra>0):            
            get_stats_estimations(intra_dc_migrations)

        mape,rmse = calculate_mape_rmse(migs_data)
    
    return cont_inter,cont_intra,mape,rmse

def calculate_mape_rmse(data):
    n = 0
    sum_ape = 0
    sum_rmse =0
    
    for values in data:
        n = n + 1
    
        real_value = values[0]
        estimated_value = values[1]
        absolute_val = abs(real_value - estimated_value)        
        
        sum_rmse += (real_value - estimated_value) **2
        sum_ape += (absolute_val/real_value) * 100
    mape = sum_ape/n
    rmse = math.sqrt(sum_rmse/n)
    print("#mape  ",mape,"#rmse  ",rmse,n)
    return mape,rmse

def update_summary_of_results(algorithm_used, workload_used, deployment_used, platform_used,total_energy,brown_energy,green_energy,migs_inter,migs_intra):    
    
    header = "algorithm;workload;platform;deployment;total_energy_consumed;brown_energy_consumed;green_energy_consumed;green_energy_utilization;number_migs_inter;number_migs_intra;underestimated_migs_inter; underestimated_migs_intra;mape;rmse;wasted_energy_origin; wasted_energy_target;\n"    
    summary_file = open("results/summary_results.csv", "w")        
    green_energy_usage = (total_energy - brown_energy ) / green_energy

    # Generating the header
    summary_file.write(header)
    summary_file.write(algorithm_used+csv_separator+workload_used+csv_separator+platform_used+csv_separator+deployment_used+csv_separator+str(total_energy) + csv_separator+str(brown_energy)+csv_separator+str(green_energy)+csv_separator+str(green_energy_usage)+str(migs_inter)+csv_separator+str(migs_intra)+csv_separator+csv_separator+csv_separator+csv_separator+csv_separator+csv_separator+csv_separator )
    summary_file.close()


analise_migrations_no_congestion("results/migrations_data_no_congestion.csv")
analise_estimations('results/migrations.csv')


def update_results_file(wasted_brown,wasted_green,max_abs,avg_abs,max_rel,avg_rel,underestimated_inter,underestimated_intra, mape,rmse):
    
    results_file_path = "results/summary_results.csv"                    
    line_to_write =""
    header = "algorithm;workload;platform;deployment;total_energy_consumed;brown_energy_consumed;green_energy_consumed;green_energy_utilization;number_migs_inter;number_migs_intra;max_abs;avg_abs;max_rel;avg_rel;underestimated_migs_inter; underestimated_migs_intra;mape;rmse;wasted_energy_origin; wasted_energy_target;\n"    
    with open(results_file_path, encoding="UTF-8") as results_file:                
        for line_number, line in enumerate(results_file):  
            if(line_number<1 ) :
                print(line)
                continue
            row = line.split(";")            
            algorithm_used            = row[0]
            workload_used             = row[1]
            deployment_used           = row[2]
            platform_used             = row[3]
            total_energy              = row[4]
            brown_energy              = row[5]
            green_energy              = row[6]
            green_energy_used         = row[7]
            migs_inter                = row[8]
            migs_intra                = row[9]
            line_to_write = algorithm_used+csv_separator+workload_used+csv_separator+platform_used+csv_separator+deployment_used+csv_separator+total_energy+csv_separator+brown_energy+csv_separator+green_energy+csv_separator+green_energy_used+csv_separator+migs_inter+csv_separator+migs_intra+csv_separator+str(max_abs)+csv_separator+str(avg_abs)+csv_separator+str(max_rel)+csv_separator+str(avg_rel) +csv_separator+str(underestimated_inter)+csv_separator+str(underestimated_intra)+csv_separator+str(mape)+csv_separator+str(rmse)+csv_separator+str(wasted_brown)+csv_separator+str(wasted_green)+linebreak
        
        summary_file = open(results_file_path, "w")        
        summary_file.write(header)
        summary_file.write(line_to_write)
        print("############ Done processing! Results have been saved to the file 'results/summary_results.csv' ############")

def process_migrations():
    wasted_brown   = 0.0     
    wasted_green   = 0.0      
    max_abs        = 0.0
    avg_abs        = 0.0
    max_rel        = 0.0
    avg_rel        = 0.0
    underestimated_inter =0
    underestimated_intra = 0
    mape  = 0.0
    rmse  = 0.0
    wasted_brown,wasted_green,max_abs,avg_abs,max_rel,avg_rel =  analise_migrations_no_congestion("results/migrations_data_no_congestion.csv")
    underestimated_inter,underestimated_intra, mape,rmse      =  analise_estimations('results//migrations.csv')
    update_results_file(wasted_brown,wasted_green,max_abs,avg_abs,max_rel,avg_rel,underestimated_inter,underestimated_intra, mape,rmse)

process_migrations()
