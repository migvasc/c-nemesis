#!/bin/bash

#Script for running the simulations.

#Creating the directory for this simulation
rm -rf "results_nemesis_azure"
#Creating the directory for this simulation
mkdir "results_nemesis_azure"

#cd "results_nemesis_azure"

#Creating the output directory
mkdir "results_nemesis_azure/output"

#Creating the results directory
mkdir "results_nemesis_azure/results"

#Code to run the simulations
clear && clear && simulationmain input/platform/homogeneousGrid5000Pstate.xml input/deploy/grid5000_deploy_nemesis_azure.xml input/workload/azure_2020.txt  --log=root.app:file:results_nemesis_azure/output/logfile.log

cd results_nemesis_azure
pwd
#Script for pre-processing the data
python3 ../scripts/extract_mig_data.py 

#Running the simulation of the perfect migrations
migrations_no_congestion ../input/platform/homogeneousGrid5000Pstate.xml
cd results_nemesis_azure
#Extracting the migration data to finish processing the results
python3 ../scripts/process_migrations.py
