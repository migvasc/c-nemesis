import os

###############################################  Constants
#To use in all the methods
csv_separator = ';'
linebreak = '\n'
baseline_1 = 'Baseline_1'
############################################### Methods

def convert_joules_to_WattHour(value):
    return value / (60.0*60.0) #60 seconds * 60 minutes

def extract_energy_consumption(output_folder):
    filename = output_folder + 'global_energy.csv'
    with open(filename, encoding="UTF-8") as file:            
        cursor = 0        
        for line_number, line in enumerate(file):                    
            row = line.split(';')                
            time = float(row[0])
            total_energy_consumed = convert_joules_to_WattHour(float(row[1]))
            green_energy_produced = convert_joules_to_WattHour(float(row[2]))
            brown_energy_consumed = convert_joules_to_WattHour(float(row[3]))
            if(time >= 604200.0):
                break    

    print(f'Total energy consumed: {total_energy_consumed} Wh')
    print(f'Green energy produced: {green_energy_produced} Wh')
    print(f'Brown energy consumed: {brown_energy_consumed} Wh')
    return total_energy_consumed,brown_energy_consumed, green_energy_produced
        
def extract_migrations_times(workload):
    vms_core = {}
    
    with open(workload, encoding="UTF-8") as traces_file:    
        cursor = 0
        for line_number, line in enumerate(traces_file):        
            row = line.split(' ')
            if len(row) < 3:
                continue
            if row[3] =='':
                continue
            else:                  
                vm_id = row[3]
                amount_cpu_cores = row[4]
                vms_core[vm_id] = amount_cpu_cores           
            cursor += len(line)    
            
    migrations_file =  'results/migrations.csv'
    file_to_write ='results/migrations_data_to_process.csv'         
    f = open(file_to_write, "w")
    with open(migrations_file, encoding="UTF-8") as mig_file:    
        cursor = 0        
        for line_number, line in enumerate(mig_file):                        
            #First line is the header
            if(line_number == 0):
                continue

            row        = line.split(csv_separator)
            vm_id      = row[0]
            source     = row[1]
            dest       = row[2]
            duration   = float(row[5])            
            estimation = float(row[7])        
            line_to_write = vms_core[vm_id] + csv_separator + vm_id + csv_separator+ source + csv_separator+ dest + csv_separator+ str(duration) + csv_separator+ str(estimation) + linebreak
            f.write(line_to_write)
            cursor += len(line)               
    f.close()
    print(f'Generated migration times to compare with no congestion!')


def extract_migrations_data(output_folder):
    filename = output_folder + 'logfile.log'
    mig_dict = {} #dictionary for storing the migrations info, key is the vm id, and value is the migration data    
    mig_file = open("results/migrations.csv", "w")
    
    # Generating the header
    mig_file.write('vm_id;source;dest;start;finish;duration;worse_estimation;best_estimation;mig_finished\n')
    
    cont_inter_dc =0
    cont_intra_dc =0
    cont_errors  = 0
    rows_written = 0
            
    with open(filename, encoding="UTF-8") as log_file:            
        cursor = 0        
        for line_number, line in enumerate(log_file):   
            row = line.split('#')            
            if(line_number < 2  ):
                continue
            # Custom output from the LOG
            if len(row) > 1 :                
                row = row[1].split(csv_separator)                
                
                # Migration Error
                if row[0] == "ME":                     
                    cont_errors+=1
                    vm_id = row[1].replace(linebreak,'')                    
                    row = mig_dict[vm_id]
                    full_row = line.split(' ')                    
                    vm_id            = row[2]
                    source           = row[3]
                    dest             = row[4]
                    start            = str( float(row[1]) + float(row[8].replace(linebreak,'')))
                    finish           = full_row[1].replace(']','')  
                    duration         = str( float(finish) - float(start))
                    worse_estimation = row[6]
                    best_estimation  = row[5]                   
                    mig_finished     = '0'
                    migration_row    = vm_id + csv_separator+ source + csv_separator+ dest +csv_separator+ start + csv_separator+ finish +csv_separator + duration+ csv_separator+  worse_estimation + csv_separator+ best_estimation + csv_separator + mig_finished + linebreak
                    #del mig_dict[vm_id]
                    mig_file.write(migration_row)    
                    rows_written +=1
                    
                # Migration Planned
                elif row[0] == "MP": 
                    vm_id = row[2]
                    mig_dict[vm_id]  = row

                # Migration Finished                                        
                elif row[0] == "MF": 
                    vm_id            = row[2]
                    source           = row[3]
                    dest             = row[4]
                    start            = row[5]
                    finish           = row[6]
                    duration         = row[7]
                    worse_estimation = row[8]
                    best_estimation  = row[9].replace(linebreak,'')                   
                    mig_finished     = '1'
                    migration_row    = vm_id + csv_separator+ source + csv_separator+ dest +csv_separator+ start + csv_separator+ finish +csv_separator + duration+ csv_separator+  worse_estimation + csv_separator+ best_estimation + csv_separator + mig_finished + linebreak                    
                    mig_file.write(migration_row)  
                    rows_written += 1                             
                    dc_origin =   source.split('.')[1]
                    dc_dest   =   dest.split('.')[1]                                        
                    if dc_origin == dc_dest:
                        
                        cont_intra_dc +=1
                    else:
                        cont_inter_dc+=1
           
                    
        cursor += len(line)                        
    mig_file.close()    
    print(f'Done ! File generated: {os.path.realpath(mig_file.name)}')        
    print(f'Migrations executed Inter DC: {cont_inter_dc}; Intra DC: {cont_intra_dc}; Migrations with errors: {cont_errors}')        
    return cont_inter_dc,cont_intra_dc


def write_summary_of_results(algorithm_used, workload_used, deployment_used, platform_used,total_energy,brown_energy,green_energy,migs_inter,migs_intra):            
    underestimated_migs_inter = 0
    underestimated_migs_intra = 0 
    mape =                      0
    rmse =                      0 
    wasted_energy_origin =      0
    wasted_energy_target =      0 
    max_abs        = 0.0
    avg_abs        = 0.0
    max_rel        = 0.0
    avg_rel        = 0.0    
    header = "algorithm;workload;platform;deployment;total_energy_consumed;brown_energy_consumed;green_energy_consumed;green_energy_utilization;number_migs_inter;number_migs_intra;max_abs;avg_abs;max_rel;avg_rel;underestimated_migs_inter; underestimated_migs_intra;mape;rmse;wasted_energy_origin; wasted_energy_target;\n"    
    summary_file = open("results/summary_results.csv", "w")        
    green_energy_usage = (total_energy - brown_energy ) / green_energy

    # Generating the header
    summary_file.write(header)
    line_to_write =  algorithm_used + csv_separator + workload_used + csv_separator + platform_used + csv_separator + deployment_used + csv_separator + str(total_energy)+ csv_separator + str(brown_energy) + csv_separator + str(green_energy) + csv_separator + str(green_energy_usage) +csv_separator+ str(migs_inter) + csv_separator+  str(migs_intra)+csv_separator+str(max_abs)+csv_separator+str(avg_abs)+csv_separator+str(max_rel)+csv_separator+str(avg_rel)+csv_separator+str(underestimated_migs_inter) +csv_separator+str(underestimated_migs_intra)+csv_separator+str(mape) + csv_separator + str(rmse) + csv_separator +  str(wasted_energy_origin) + csv_separator+str(wasted_energy_target) + linebreak 
    summary_file.write(line_to_write)
    summary_file.close()



def processLogFile(main_directory):
    output_folder = main_directory + 'output/'
    filename = output_folder + 'logfile.log'
    algorithm_used = ''
    workload_used = ''
    deployment_used = ''
    platform_used = ''        
    migs_inter = 0
    migs_intra = 0
    total_energy_consumed = 0.0
    brown_energy_consumed = 0.0
    green_energy_produced = 0.0
    with open(filename, encoding="UTF-8") as log_file:            
        cursor = 0        
        for line_number, line in enumerate(log_file):   
            row = line.split('#')            
            if(line_number == 0 ):
                row = line.split(':')
                platform_used   = row[1].split(';')[0].strip()
                deployment_used =  row[2].split(';')[0].strip()
                workload_used = row[3].replace('\n','').strip()
                print(platform_used,deployment_used,workload_used)
                
            elif(line_number  == 1 ):
                row = row[1].split(':')
                algorithm_used = row[1].strip()
                print(algorithm_used)
                
            else:
                break
                
    if (algorithm_used != baseline_1):
        migs_inter,migs_intra = extract_migrations_data(output_folder)        
        workload_file =  '../'+workload_used
        extract_migrations_times(workload_file)    

    total_energy_consumed, brown_energy_consumed,green_energy_produced = extract_energy_consumption(output_folder)
    write_summary_of_results(algorithm_used, workload_used, deployment_used, platform_used,total_energy_consumed, brown_energy_consumed,green_energy_produced,migs_inter,migs_intra)

processLogFile("")
