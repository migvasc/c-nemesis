{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-0f471c78a4525d5195f02aedbe232349503f88f7.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulations_schedule_param
    tinypkgs.migrations_no_congestion
  ];
}