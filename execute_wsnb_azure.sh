#!/bin/bash

#Script for running the simulations.

#Creating the directory for this simulation
rm -rf "results_wsnb_azure"
#Creating the directory for this simulation
mkdir "results_wsnb_azure"

#cd "results_wsnb_azure"

#Creating the output directory
mkdir "results_wsnb_azure/output"

#Creating the results directory
mkdir "results_wsnb_azure/results"

#Code to run the simulations
clear && clear && simulationmain input/platform/homogeneousGrid5000Pstate.xml input/deploy/grid5000_deploy_wsnb_azure.xml input/workload/azure_2020.txt  --log=root.app:file:results_wsnb_azure/output/logfile.log

cd results_wsnb_azure
pwd

#Script for pre-processing the data
python3 ../scripts/extract_mig_data.py 